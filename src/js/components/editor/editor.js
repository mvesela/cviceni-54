import Component from '../component';
import SelectionMenu from './selectionMenu';
import WrittingMenu from './writtingMenu';
import schemaConfig from './schema';
import { __dispatchEvent } from '../../lib/utils';

export default class Editor extends Component {
  constructor($element) {
    super($element);
    this.editor = undefined;
    this.id = this.target.id;
    this.editorType = this.target.getAttribute('data-editor');
    // find editor place in cviceni
    this.$editor = this.target.querySelector(`#prosemirror-editor-${this.id}`);
    // find editor content
    this.$content = this.target.querySelector(`#prosemirror-content-${this.id}`);
    this.schema = new pm.Schema(schemaConfig);

    // only if cviceni isn't done load html
    if (!HISTORYLAB.import.done) {
      this.initHTML();
    }
    else {
      this.initJSON();
    }
  }

  // calling from save-data.js
  getEditorJSON() {
    if (this.editor) {
      // get JSON format of last editor state
      return this.editor.state.toJSON();
    }
    return {};
  }

  // type of editor is set here
  initHTML() {
    let plugins;
    // add plugin by editor type
    if (this.editorType === 'zvyraznovani') {
      const bubbleMenu = this.selectionMenu(this.id);
      plugins = [bubbleMenu];
    }

    if (this.editorType === 'predznaceny') {
      const premarkedParts = Editor.premarkedParts(this.id, this.schema);
      plugins = [premarkedParts];
    }

    if (this.editorType === 'psani') {
      const writting = this.writting(pm.keydownHandler);
      const histKeymap = pm.keymap({ 'Mod-z': pm.undo, 'Mod-y': pm.redo });
      plugins = [pm.history(), histKeymap, writting];
    }

    // create editor instance by defining editor view
    this.editor = new pm.EditorView(this.$editor, {
      // state is created from html structure parsed from our text and array of plugins
      state: pm.EditorState.create({
        doc: pm.DOMParser.fromSchema(this.schema).parse(this.$content),
        plugins,
      }),
    });
  }

  // only visualization of saved data
  // load saved data from JSON format
  initJSON() {
    const data = JSON.parse(this.$content.textContent);
    const readOnly = Editor.readOnlyMod();
    const { schema } = this;

    // create editor instance by defining editor view
    this.editor = new pm.EditorView(this.$editor, {
      // editor state is created from JSON object and schema
      state: pm.EditorState.fromJSON({ schema, plugins: [readOnly] }, data),
    });
  }

  // only diable editing editor
  // https://prosemirror.net/docs/ref/#view.EditorProps.editable
  static readOnlyMod() {
    return new pm.Plugin({
      props: {
        editable() {
          return false;
        },
      },
      // plugin key has to be unique in editor
      key: new pm.PluginKey('ReadOnlyMod'),
    });
  }

  // manager of marker
  selectionMenu(editorId) {
    const { schema } = this;
    // get menu from gulp
    const $menuTemplate = this.target.querySelector(`#marker-menu-${this.target.id}`);
    return new pm.Plugin({
      // set plugin visualization means set definated marker menu as part of plugin
      view(editorView) {
        return new SelectionMenu(editorId, schema, editorView, $menuTemplate);
      },
      // when editable is false history doesn't work
      props: {
        editable() {
          return false;
        },
      },
      // plugin key has to be unique in editor
      key: new pm.PluginKey('SelectionMenu'),
    });
  }

  // manager of premarker
  static premarkedParts(editorId, schema) {
    return new pm.Plugin({
      props: {
        // when editable is false history doesn't work
        editable() {
          return false;
        },
        // https://prosemirror.net/docs/ref/#view.EditorProps.handleClickOn
        handleClickOn(view, pos, node, nodePos) {
          if (schema.nodes.premarked.name === node.type.name) {
            // definition of class adding to selected node
            const classSelectDefinition = 'selected';
            const { state } = view;
            const { tr } = state;

            let newClassName;
            let task;
            // variable for extra data from cviceni JSON for dispatch event distribution
            const tagData = JSON.parse(node.attrs.textData);
            tagData.tagName = 'vyznacena-pasaz';

            if (node.attrs.className.includes(classSelectDefinition)) {
              newClassName = node.attrs.className.replace(` ${classSelectDefinition}`, '');
              task = 'remove';
            }
            else {
              newClassName = `${node.attrs.className} ${classSelectDefinition}`;
              task = 'add';
            }

            // for create new state it's important to not change html node directly
            // it's important to use prosemirror functions
            const nodeAttrs = {
              id: node.attrs.id,
              className: newClassName,
              textData: node.attrs.textData,
            };

            // replace old node on set position by new node with new attributes
            tr.setNodeMarkup(nodePos, null, nodeAttrs);

            // changing editor's state
            const newState = state.apply(tr);
            view.updateState(newState);

            // dispatch event that editor has changed
            const $node = view.dom.querySelector(`#${node.attrs.id}`);
            __dispatchEvent(document, 'editor.change', {}, {
              text: {
                id: editorId,
                task,
                tagData,
                value: $node.cloneNode(true),
              },
            });
          }
          return false;
        },
      },
      // plugin key has to be unique in editor
      key: new pm.PluginKey('PremarkedParts'),
    });
  }

  // manager of writting
  writting(keydownHandler) {
    // get menu items from gulp
    const $toolbarTemplate = this.target.querySelector(`#toolbar-${this.target.id}`);
    // console.log($toolbarTemplate);
    let sugar = 0;

    // definition of deleted parts logic
    const deletedParts = (state, dispatch) => {
      // create unique id
      const id = `remove-part-${sugar}`;
      // define command
      const command = pm.toggleMark(this.schema.marks.writtingRemovePart, { id });
      // marked selection part
      command(state, dispatch);
      // increase sugar
      sugar += 1;
      return true;
    };

    return new pm.Plugin({
      // set plugin visualization means set definated writting menu as part of plugin
      view(editorView) {
        return new WrittingMenu(editorView, $toolbarTemplate);
      },
      props: {
        handleKeyDown: keydownHandler({ Delete: deletedParts, Backspace: deletedParts }),
      },
      // plugin key has to be unique in editor
      key: new pm.PluginKey('Writting'),
    });
  }
}
